rootProject.name = "libs-versions"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
    }
}
