plugins {
    `maven-publish`
    `version-catalog`
}

catalog {
    versionCatalog {
        from(files("./libs.versions.toml"))
    }
}

publishing {
    publications {
        create<MavenPublication>("versionCatalog") {
            groupId = "com.zustematic.versions"
            artifactId = "libs"
            version = "0.0.2"
            from(components["versionCatalog"])
        }
    }
    repositories {
        val gitlabUrl = System.getenv("CI_API_V4_URL")
        val gitlabProjectId = System.getenv("CI_PROJECT_ID")
        if (gitlabUrl != null && gitlabProjectId != null) {
            maven {
                url = java.net.URI("$gitlabUrl/projects/$gitlabProjectId/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}